function createCard(name, description, pictureUrl, startDate, endDate, location) {
    return `
    <div class="col mt-3">
    <div class="card shadow">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h class="card-subtitle mb-2 text-muted">${location}</h>
          <p class="card-text">${description}</p>
          <div class-"card-footer text-body-secondary">${startDate} - ${endDate}</div>
        </div>
        </div>
      </div>
    `;
  }


  function triggerAlert() {
    return `
    <div class="alert alert-warning" role="alert">
    Error!
      </div>
    `;
  }


  window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
      const response = await fetch(url);

      if (!response.ok) {
        // Figure out what to do when the response is bad
        let badAlert = document.querySelector('h2')
        badAlert.innerHTML += triggerAlert()
      } else {
        const data = await response.json();

        for (let conference of data.conferences) {
          const detailUrl = `http://localhost:8000${conference.href}`;
          const detailResponse = await fetch(detailUrl);
          if (detailResponse.ok) {
            const details = await detailResponse.json();
            const startDate = new Date(details.conference.starts).toLocaleDateString('en');
            const endDate = new Date(details.conference.ends).toLocaleDateString('en');
            const name = details.conference.name;
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;
            const location = details.conference.location.name;
            const html = createCard(name, description, pictureUrl, startDate, endDate, location);
            const column = document.querySelector('.row');
            column.innerHTML += html;
          }
        }

      }
    } catch (e) {
      // Figure out what to do if an error is raised
      let badAlert = document.querySelector('h5')
      badAlert.innerHTML += triggerAlert()
    }

  });
